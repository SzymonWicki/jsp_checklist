package servlets;

import database.EntityDao;
import model.Checklist;
import model.ChecklistItem;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/checklistitem/list")
public class ChecklistItemListServlet extends HttpServlet {

    private EntityDao entityDao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");

        if (id != null) {
            final Optional<Checklist> optionalChecklist = entityDao.findById(Checklist.class, Long.parseLong(id));
            if (optionalChecklist.isPresent()) {
                req.setAttribute("checklistitem", optionalChecklist.get().getChecklistItems());
            } else {
                System.err.println("Not found this checklist Items");
            }
        } else {
            req.setAttribute("checklistitem", entityDao.findAll(ChecklistItem.class));
        }

        req.getRequestDispatcher("/checklistitemlist.jsp").forward(req, resp);

    }
}

