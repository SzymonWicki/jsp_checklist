package servlets;

import database.EntityDao;
import model.Checklist;
import model.ChecklistItem;
import model.Status;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/checklist/list")
public class CheckListServlet extends HttpServlet {

    private EntityDao entityDao = new EntityDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");

        if (id != null) {
            final Optional<Checklist> optionalChecklist = entityDao.findById(Checklist.class, Long.parseLong(id));
            if (optionalChecklist.isPresent()) {
                req.setAttribute("checklist", optionalChecklist.get().getChecklistItems());
            } else {
                System.err.println("Not found this checklist");
            }
        } else {
            req.setAttribute("checklist", entityDao.findAll(Checklist.class));
        }

        req.getRequestDispatcher("/checklist.jsp").forward(req, resp);

    }


}
