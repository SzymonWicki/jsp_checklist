<%@ page import="model.Status" %><%--
  Created by IntelliJ IDEA.
  User: Szymon
  Date: 21.07.2019
  Time: 14:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Checklist form</title>
</head>
<body>
<jsp:include page="/header.jsp"></jsp:include>

<form action="/checklistitem/add" method="post">
    Item: <input type="hidden" name="id" value="<%=request.getAttribute("id")%>" readonly>

    <input type="text" name="name" placeholder="Checklist Name">
    <input type="text" name="description" placeholder="Checklist Description">
    <select name="status">
        <%
            for (Status value : Status.values()) {
                out.println("<option value=" + "\"" + value + "\"" + ">" + value + "</option>");
            }

        %>
    </select>
    <br>
    <br>
    <input type="submit" value="Submit">

</form>
</body>
</html>
