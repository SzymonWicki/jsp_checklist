<%@ page import="model.Checklist" %>
<%@ page import="java.util.List" %>
<%@ page import="model.ChecklistItem" %><%--
  Created by IntelliJ IDEA.
  User: Szymon
  Date: 21.07.2019
  Time: 14:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ChecklistItem List</title>
</head>
<body>
<table>
    <tr>
        <th style="width: 150px;">Name</th>
        <th style="width: 150px;">Description</th>
        <th style="width: 150px;">Status</th>
    </tr>
    <%
        List<ChecklistItem> checklistsitems = (List<ChecklistItem>) request.getAttribute("checklistitem");

        for (ChecklistItem checklistItem : checklistsitems) {
            out.print("<tr>");
            out.print("<td>" + checklistItem.getName() + "</td>");
            out.print("<td>" + checklistItem.getDescription() + "</td>");
            out.print("<td>" + checklistItem.getStatus() + "</td>");
            out.print("</tr>");
        }

    %>
</table>
</body>
</html>
